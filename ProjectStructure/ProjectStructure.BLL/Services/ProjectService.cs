﻿using ProjectStructure.DAL.Interfaces;
using AutoMapper;
using System.Collections.Generic;
using ProjectStructure.DAL.Models;

namespace ProjectStructure.BLL.Services
{
	class ProjectService
	{
		private readonly IUnitOfWork data;
		private readonly IMapper mapper;

		public ProjectService(IUnitOfWork data, IMapper mapper)
		{
			this.data = data;
			this.mapper = mapper;
		}

	}
}
