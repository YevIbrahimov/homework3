﻿using System;
using System.Collections.Generic;

namespace ProjectStructure.BLL.DTO
{
	public class TeamDTO
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public DateTime CreatedAt { get; set; }
		public List<UserDTO> Users { get; set; }
	}
}
