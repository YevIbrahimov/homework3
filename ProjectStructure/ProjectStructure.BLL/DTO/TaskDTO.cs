﻿using ProjectStructure.DAL.Models;
using System;

namespace ProjectStructure.BLL.DTO
{
	public class TaskDTO
	{
		public int Id { get; set; }
		public int ProjectId { get; set; }
		public ProjectDTO Project { get; set; }
		public int PerformerId { get; set; }
		public UserDTO Performer { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public TaskStates State { get; set; }
		public DateTime CreatedAt { get; set; }
		public DateTime? FinishedAt { get; set; }
	}
}
