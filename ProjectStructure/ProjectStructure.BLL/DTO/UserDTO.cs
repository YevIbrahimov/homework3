﻿using System;
using System.Collections.Generic;

namespace ProjectStructure.BLL.DTO
{
	public class UserDTO
	{
		public int Id { get; set; }
		public int? TeamId { get; set; }
		public TeamDTO Team { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Email { get; set; }
		public DateTime RegisteredAt { get; set; }
		public DateTime BirthDay { get; set; }
		public List<TaskDTO> Tasks { get; set; }
	}
}
