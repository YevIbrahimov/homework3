﻿using ProjectStructure.DAL.Models;
using System.Collections.Generic;

namespace ProjectStructure.BLL.Interfaces
{
	interface ITaskService
	{
		List<TaskDTO> GetAll();
		TaskDTO Get();
		TaskDTO Create();
		TaskDTO Update();
		TaskDTO Delete();
	}
}
