﻿using ProjectStructure.DAL.Models;
using System.Collections.Generic;

namespace ProjectStructure.BLL.Interfaces
{
	interface IProjectService
	{
		List<ProjectDTO> GetAll();
		ProjectDTO Get();
		ProjectDTO Create();
		ProjectDTO Update();
		ProjectDTO Delete();
	}
}
