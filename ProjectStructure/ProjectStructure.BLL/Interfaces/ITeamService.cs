﻿using ProjectStructure.DAL.Models;
using System.Collections.Generic;

namespace ProjectStructure.BLL.Interfaces
{
	interface ITeamService
	{
		List<TeamDTO> GetAll();
		TeamDTO Get();
		TeamDTO Create();
		TeamDTO Update();
		TeamDTO Delete();
	}
}