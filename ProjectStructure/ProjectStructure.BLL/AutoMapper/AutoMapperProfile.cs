﻿using AutoMapper;
using ProjectStructure.BLL.DTO;
using ProjectStructure.DAL.Models;

namespace ProjectStructure.BLL.AutoMapper
{
	class AutoMapperProfile : Profile
	{
		public AutoMapperProfile()
		{
			CreateMap<Project, ProjectDTO>();
			CreateMap<ProjectDTO, Project>();
			CreateMap<Team, TeamDTO>();
			CreateMap<TeamDTO, Team>();
			CreateMap<Task, TaskDTO>();
			CreateMap<TaskDTO, Task>();
			CreateMap<User, UserDTO>();
			CreateMap<UserDTO, User>();
		}
	}
}
