﻿namespace ProjectStructure.DAL.Interfaces
{
	public interface IUnitOfWork
	{
		public IProjectRepository projectRepository { get; }
		public ITeamRepository teamRepository { get; }
		public ITaskRepository taskRepository { get; }
		public IUserRepository userRepository { get; }

	}
}
