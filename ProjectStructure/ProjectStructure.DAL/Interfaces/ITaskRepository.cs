﻿using ProjectStructure.DAL.Models;
using System.Collections.Generic;

namespace ProjectStructure.DAL.Interfaces
{
	public interface ITaskRepository
	{
		List<Task> GetAll();

		Task Get(int id);

		Task Create(Task task);

		Task Update(int id, Task task);

		void Delete(Task task);
	}
}
