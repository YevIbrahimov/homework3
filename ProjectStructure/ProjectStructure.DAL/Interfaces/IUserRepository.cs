﻿using ProjectStructure.DAL.Models;
using System.Collections.Generic;

namespace ProjectStructure.DAL.Interfaces
{
	public interface IUserRepository
	{
		List<User> GetAll();

		User Get(int id);

		User Create(User user);

		User Update(int id, User user);

		void Delete(User user);
	}
}
