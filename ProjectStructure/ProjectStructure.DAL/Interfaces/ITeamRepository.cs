﻿using ProjectStructure.DAL.Models;
using System.Collections.Generic;

namespace ProjectStructure.DAL.Interfaces
{
	public interface ITeamRepository
	{
		List<Team> GetAll();
		Team Get(int id);

		Team Create(Team team);

		Team Update(int id, Team team);

		void Delete(Team team);
	}
}
