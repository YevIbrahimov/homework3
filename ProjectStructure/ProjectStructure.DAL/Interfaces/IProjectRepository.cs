﻿using ProjectStructure.DAL.Models;
using System.Collections.Generic;

namespace ProjectStructure.DAL.Interfaces
{
	public interface IProjectRepository
	{
		List<Project> GetAll();

		Project Get(int id);

		Project Create(Project project);

		Project Update(int id, Project project);

		void Delete(Project project);
	}
}
