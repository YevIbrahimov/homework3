﻿using System;
using System.Collections.Generic;

namespace ProjectStructure.DAL.Models
{
	public class Team
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public DateTime CreatedAt { get; set; }
		public List<User> Users { get; set; }
	}
}
