﻿namespace ProjectStructure.DAL.Models
{
    public enum TaskStates
    {
        ToDo,
        InProgress,
        Done,
        Canceled
    }
}
