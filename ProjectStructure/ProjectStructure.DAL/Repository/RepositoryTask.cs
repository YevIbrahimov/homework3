﻿using Newtonsoft.Json;
using ProjectStructure.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace ProjectStructure.DAL.Repository
{
	class RepositoryTask
	{
		private static readonly List<Task> tasks;
		static RepositoryTask() => tasks = new List<Task>
		{
			new Task()
			{
				Id = 2,
				ProjectId = 2,
				PerformerId = 66,
				Name = "product Direct utilize",
				Description = "Eum a eum.",
				State = (TaskStates)2,
				CreatedAt = DateTime.Parse("2019-02-15T15:06:52.0600666+00:00"),
				FinishedAt = null
			},
			new Task()
			{
				Id = 3,
				ProjectId = 2,
				PerformerId = 75,
				Name = "bypass",
				Description = "Sint voluptatem quas.",
				State = (TaskStates)2,
				CreatedAt = DateTime.Parse("2017-08-16T06:13:44.5773845+00:00"),
				FinishedAt = DateTime.Parse("2020-09-09T06:34:47.460216+00:00")
			},
			new Task()
			{
				Id = 4,
				ProjectId = 2,
				PerformerId = 85,
				Name = "withdrawal contextually-based",
				Description = "Delectus quibusdam id quia iure neque maiores molestias sed aut.",
				State = (TaskStates)2,
				CreatedAt = DateTime.Parse("2018-10-19T00:58:34.8045103+00:00"),
				FinishedAt = null
			},
			new Task()
			{
				Id = 5,
				ProjectId = 2,
				PerformerId = 81,
				Name = "mobile Organized",
				Description = "Earum blanditiis repellendus qui magni aliquam quisquam consequatur odio ducimus.",
				State = (TaskStates)2,
				CreatedAt = DateTime.Parse("2018-06-15T06:03:48.0732466+00:00"),
				FinishedAt = null
			},
			new Task()
			{
				Id = 6,
				ProjectId = 2,
				PerformerId = 109,
				Name = "world-class Circles",
				Description = "Reiciendis iusto rerum non et aut eaque.",
				State = (TaskStates)2,
				CreatedAt = DateTime.Parse("2020-05-21T14:56:53.8117818+00:00"),
				FinishedAt = null
			},
			new Task()
			{
				Id = 7,
				ProjectId = 2,
				PerformerId = 87,
				Name = "Automotive & Tools transitional bifurcated",
				Description = "Et rerum ad.",
				State = (TaskStates)2,
				CreatedAt = DateTime.Parse("2018-07-10T16:21:12.0886153+00:00"),
				FinishedAt = null
			},
		};
		

		public List<Task> GetAll()
		{
			return tasks;
		}

		public Task Get(int id)
		{
			return tasks.Where(p => p.Id == id).First();
		}

		public Task Create(Task task)
		{
			tasks.Add(task);
			return task;
		}

		public Task Update(int id, Task task)
		{
			var taskToDelete = tasks.Where(p => p.Id == id).First();
			tasks.Remove(taskToDelete);
			tasks.Add(task);

			return task;
		}

		public void Delete(Task task)
		{
			tasks.Remove(task);
		}
	}
}
