﻿using ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.DAL.Repository
{
	class UnitOfWork : IUnitOfWork
	{
		private readonly IProjectRepository adProjectRepository;
		private readonly IUserRepository adUserRepository;
		private readonly ITeamRepository adTeamRepository;
		private readonly ITaskRepository adTaskRepository;

		public UnitOfWork(IProjectRepository adProjectRepository, IUserRepository adUserRepository, ITeamRepository adTeamRepository, ITaskRepository adTaskRepository)
		{
			this.adProjectRepository = adProjectRepository;
			this.adTaskRepository = adTaskRepository;
			this.adTeamRepository = adTeamRepository;
			this.adUserRepository = adUserRepository;
		}

		public IProjectRepository projectRepository { get => adProjectRepository; }
		public ITeamRepository teamRepository { get => adTeamRepository; }
		public ITaskRepository taskRepository { get => adTaskRepository; }
		public IUserRepository userRepository { get => adUserRepository; }

	}
}
