﻿using System.Collections.Generic;
using System.Net.Http;
using System.Linq;
using Newtonsoft.Json;
using ProjectStructure.DAL.Models;
using ProjectStructure.DAL.Interfaces;
using System;

namespace ProjectStructure.DAL.Repository
{
	class RepositoryProject : IProjectRepository
	{
		private static readonly List<Project> projects;
		static RepositoryProject() => projects = new List<Project>()
			{
				new Project()
				{
					Id = 0,
					AuthorId = 26,
					TeamId = 10,
					Name = "open architecture Outdoors, Grocery & Baby Dynamic",
					Description = "Repellendus expedita dolorum saepe ut culpa nobis sunt itaque labore.",
					Deadline = DateTime.Parse("2021-08-03T01:08:10.3228394+00:00"),
					CreatedAt =  DateTime.Parse("2019-07-17T06:42:48.376246+00:00")
				},
				new Project()
				{
					Id = 1,
					AuthorId = 22,
					TeamId = 0,
					Name = "backing up Handcrafted Fresh Shoes challenge",
					Description = "Et doloribus et temporibus.",
					Deadline = DateTime.Parse("2021-09-12T19:17:47.2335223+00:00"),
					CreatedAt =  DateTime.Parse("2020-08-25T17:49:50.4518054+00:00")
				},
				new Project()
				{
					Id = 2,
					AuthorId = 31,
					TeamId = 0,
					Name = "Village",
					Description = "Non voluptatem voluptas libero.",
					Deadline = DateTime.Parse("2021-07-24T12:07:31.9357846+00:00"),
					CreatedAt =  DateTime.Parse("2021-01-30T14:38:53.8838745+00:00")
				},
				new Project()
				{
					Id = 3,
					AuthorId = 31,
					TeamId = 0,
					Name = "Dam",
					Description = "Quia et tempora hic pariatur voluptatem doloribus sunt.",
					Deadline = DateTime.Parse("2021-07-21T08:32:51.3354654+00:00"),
					CreatedAt =  DateTime.Parse("2020-03-15T20:33:15.6731141+00:00")
				},
				new Project()
				{
					Id = 4,
					AuthorId = 29,
					TeamId = 0,
					Name = "iterate project",
					Description = "Soluta non sed assumenda.",
					Deadline = DateTime.Parse("2021-06-25T11:25:00.7111264+00:00"),
					CreatedAt =  DateTime.Parse("2019-07-03T03:39:01.9978679+00:00")
				},
				new Project()
				{
					Id = 5,
					AuthorId = 29,
					TeamId = 0,
					Name = "Libyan Dinar Netherlands Antilles",
					Description = "Voluptatibus error ut id libero quam natus molestias natus.",
					Deadline = DateTime.Parse("2021-09-14T02:53:20.8003038+00:00"),
					CreatedAt =  DateTime.Parse("2021-03-15T19:47:35.9335401+00:00")
				},
				new Project()
				{
					Id = 6,
					AuthorId = 25,
					TeamId = 0,
					Name = "New Jersey capacitor program",
					Description = "Quas fuga qui eaque et corporis.",
					Deadline = DateTime.Parse("2021-09-23T03:11:42.1994133+00:00"),
					CreatedAt =  DateTime.Parse("2020-03-22T16:51:40.1965166+00:00")
				}
			};


		public List<Project> GetAll()
		{
			return projects;
		}

		public Project Get(int id)
		{
			return projects.Where(p => p.Id == id).First();
		}

		public Project Create(Project project)
		{
			projects.Add(project);
			return project;
		}

		public Project Update(int id, Project project)
		{
			var projectToDelete = projects.Where(p => p.Id == id).First();
			projects.Remove(projectToDelete);
			projects.Add(project);

			return project;
		}

		public void Delete(Project project)
		{
			projects.Remove(project);
		}

	}
}
