﻿using ProjectStructure.DAL.Interfaces;
using ProjectStructure.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.DAL.Repository
{
	class RepositoryTeam : ITeamRepository
	{

		private readonly List<Team> teams;
		public RepositoryTeam() => teams = new List<Team>()
		{
			new Team()
			{
				Id = 0,
				Name = "Denesik - Greenfelder",
				CreatedAt = DateTime.Parse("2019-08-25T15:48:06.062331+00:00")

			},
			new Team()
			{
				Id = 1,
				Name = "Durgan Group",
				CreatedAt = DateTime.Parse("2017-03-31T02:29:28.3740504+00:00")

			},
			new Team()
			{
				Id = 2,
				Name = "Kassulke LLC",
				CreatedAt = DateTime.Parse("2019-02-21T15:47:30.3797852+00:00")

			},
			new Team()
			{
				Id = 3,
				Name = "Harris LLC",
				CreatedAt = DateTime.Parse("2018-08-28T08:18:46.4160342+00:00")

			},
			new Team()
			{
				Id = 4,
				Name = "Mitchell Inc",
				CreatedAt = DateTime.Parse("2019-04-03T09:58:33.0178179+00:00")
			},
			new Team()
			{
				Id = 5,
				Name = "Smitham Group",
				CreatedAt = DateTime.Parse("2016-10-05T07:57:02.8427653+00:00")
			},
			new Team()
			{
				Id = 6,
				Name = "Kutch - Roberts",
				CreatedAt = DateTime.Parse("2016-10-31T05:05:15.1076578+00:00")

			},
			new Team()
			{
				Id = 7,
				Name = "Parisian Group",
				CreatedAt = DateTime.Parse("2016-07-17T01:34:55.0917082+00:00")
			}
		};


		public List<Team> GetAll()
		{
			return teams;
		}

		public Team Get(int id)
		{
			return teams.Where(p => p.Id == id).First();
		}

		public Team Create(Team team)
		{
			teams.Add(team);
			return team;
		}

		public Team Update(int id, Team team)
		{
			var teamToDelete = teams.Where(p => p.Id == id).First();
			teams.Remove(teamToDelete);
			teams.Add(team);

			return team;
		}

		public void Delete(Team team)
		{
			teams.Remove(team);
		}
	}
}
